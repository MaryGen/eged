﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace eged
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        DateTime current;
        private void Form1_Load(object sender, EventArgs e)
        {
            db.connect();
            try
            {
                MySqlDataReader r = db.select("select fam_v from vladelec where id_vladelec=101;");
                if (!r.Read() || r.GetValue(0) == DBNull.Value)
                {
                    db.close();
                    if ((new addVlad()).ShowDialog() != DialogResult.OK)
                        Application.Exit();
                }
                else
                    db.close();
                current = DateTime.Now;
                MakeKal();
            }
            catch {
                db.close();
                if ((new addVlad()).ShowDialog() != DialogResult.OK)
                    Application.Exit();
                else
                {
                    current = DateTime.Now;
                    MakeKal();
                }
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataGridViewSelectedRowCollection rows = dataGridView1.SelectedRows;
                db.connect();
                foreach (DataGridViewRow row in rows)
                {
                    db.exec("delete from zadacha where id_zadache=" + row.Tag.ToString() + ";");
                    dataGridView1.Rows.Remove(row);
                }
                db.close();
                dataGridView1.ClearSelection();
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text == "Календарь")
                MakeKal();
            else if (tabControl1.SelectedTab.Text == "Задачи")
                MakeZad();
            else if (tabControl1.SelectedTab.Text == "Заметки")
                MakeZam();
            else if (tabControl1.SelectedTab.Text == "Контакты")
                MakeKon();
        }

        void MakeKon()
        {
            db.connect();
            dataGridView4.Rows.Clear();
            MySqlDataReader reader = db.select("select fam_k,name_k,otch_k,tel_k,email_k,adres_k,data_rogden_k,id_kontakt from kontakt order by fam_k;");
            while (reader.Read())
            {
                string fio = reader.GetString(0) + " " + reader.GetString(1) + " " + reader.GetString(2);
                string tel = reader.GetString(3);
                string mail = reader.GetString(4);
                string address = reader.GetString(5);
                DateTime birth = reader.GetDateTime(6);
                int id = reader.GetInt32(7);
                DataGridViewRow row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells[0].Value = fio;
                row.Cells[1].Value = tel;
                row.Cells[2].Value = mail;
                row.Cells[3].Value = address;
                row.Cells[4].Value = birth.ToShortDateString();
                row.Tag = id;
                dataGridView4.Rows.Add(row);
            }
            reader.Close();
            dataGridView4.ClearSelection();
            db.close();
        }
        void MakeZad()
        {
            db.connect();
            dataGridView2.Rows.Clear();
            MySqlDataReader reader = db.select("select nazvan_z,konez_z,kategor_z,data_zav_z,id_zadache from zadacha order by prioritet_z,konez_z;");
            while (reader.Read())
            {
                string name = reader.GetString(0);
                DateTime konec = reader.GetDateTime(1);
                string kateg = reader.GetString(2);
                object zaver = reader.GetValue(3);
                int id = reader.GetInt32(4);
                DataGridViewRow row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewCheckBoxCell());
                row.Cells[0].Value = name;
                row.Cells[1].Value = konec.ToShortDateString();
                row.Cells[2].Value = kateg;
                row.Cells[3].Value = zaver != DBNull.Value;
                row.Tag = id;
                dataGridView2.Rows.Add(row);
            }
            reader.Close();
            dataGridView2.ClearSelection();
            db.close();
        }
        void MakeKal()
        {
            this.Text = "Личный ежедневник (" + current.ToShortDateString() + ")";
            db.connect();
            dataGridView1.Rows.Clear();
            MySqlDataReader reader = db.select("select nazvan_z,konez_z,kategor_z,data_zav_z,id_zadache from zadacha where (data_zav_z is null) or (STR_TO_DATE('" + current.AddDays(-1).ToShortDateString() + "', '%d.%m.%Y') < STR_TO_DATE(konez_z, '%Y-%m-%d') and STR_TO_DATE('" + current.AddDays(1).ToShortDateString() + "', '%d.%m.%Y') > STR_TO_DATE(nachalo_z, '%Y-%m-%d')) order by prioritet_z,konez_z;");
            while (reader.Read())
            {
                string name = reader.GetString(0);
                DateTime konec = reader.GetDateTime(1);
                string kateg = reader.GetString(2);
                object zaver = reader.GetValue(3);
                int id = reader.GetInt32(4);
                DataGridViewRow row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewCheckBoxCell());
                row.Cells[0].Value = name;
                row.Cells[1].Value = konec.ToShortDateString();
                row.Cells[2].Value = kateg;
                row.Cells[3].Value = zaver != DBNull.Value;
                row.Tag = id;
                dataGridView1.Rows.Add(row);
            }
            reader.Close();
            dataGridView1.ClearSelection();
            db.close();

            db.connect();
            dataGridView3.Rows.Clear();
            reader = db.select("select nazvan_m,nachalo_m,id_meropriyatie from meropriyatie where STR_TO_DATE(nachalo_m, '%Y-%m-%d')=STR_TO_DATE('" + current.ToShortDateString() + "', '%d.%m.%Y');");
            while (reader.Read())
            {
                string name = reader.GetString(0);
                DateTime nachalo = reader.GetDateTime(1);
                int id = reader.GetInt32(2);
                DataGridViewRow row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells.Add(new DataGridViewTextBoxCell());
                row.Cells[0].Value = nachalo.ToShortTimeString();
                row.Cells[1].Value = name;
                row.Tag = id;
                dataGridView3.Rows.Add(row);
            }
            reader.Close();
            dataGridView3.ClearSelection();
            db.close();
        }

        public class Nitem
        {
            public Nitem(int id, string str)
            {
                this.id = id;
                this.str = str;
            }
            public int id;
            public string str;
            public override string ToString()
            {
                return str;
            }
        }
        void MakeZam()
        {
            db.connect();
            listBox1.Items.Clear();
            MySqlDataReader reader = db.select("select text_za,id_zametka from zametka order by id_zametka;");
            while (reader.Read())
            {
                string text = reader.GetString(0);
                int id = reader.GetInt32(1);
                listBox1.Items.Add(new Nitem(id, text));
            }
            reader.Close();
            db.close();
        }

        private void dataGridView2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataGridViewSelectedRowCollection rows = dataGridView2.SelectedRows;
                db.connect();
                foreach (DataGridViewRow row in rows)
                {
                    db.exec("delete from zadacha where id_zadache=" + row.Tag.ToString() + ";");
                    dataGridView2.Rows.Remove(row);
                }
                db.close();
                dataGridView2.ClearSelection();
            }
        }
        private void dataGridView4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataGridViewSelectedRowCollection rows = dataGridView4.SelectedRows;
                db.connect();
                foreach (DataGridViewRow row in rows)
                {
                    db.exec("delete from kontakt where id_kontakt=" + row.Tag.ToString() + ";");
                    dataGridView4.Rows.Remove(row);
                }
                db.close();
                dataGridView4.ClearSelection();
            }
        }

        private void задачуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new addZad()).ShowDialog();
            MakeZad();
        }
        private void заметкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new addZam()).ShowDialog();
            MakeZam();
        }
        private void контактToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new addKon()).ShowDialog();
            MakeKon();
        }
        private void мероприятиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new addMer()).ShowDialog();
            MakeKal();
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ListBox.SelectedObjectCollection rows = listBox1.SelectedItems;
                db.connect();
                foreach (object row in rows)
                    db.exec("delete from zametka where id_zametka=" + ((Nitem)row).id.ToString() + ";");
                db.close();
                MakeZam();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            current = current.AddDays(-1);
            MakeKal();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            current = current.AddDays(1);
            MakeKal();
        }
        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (new addZam(((Nitem)listBox1.SelectedItem).id)).ShowDialog();
            MakeZam();
        }
        private void dataGridView4_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (new addKon(Convert.ToInt64(dataGridView4.SelectedRows[0].Tag))).ShowDialog();
            MakeKon();
        }
        private void dataGridView2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (new addZad(Convert.ToInt64(dataGridView2.SelectedRows[0].Tag))).ShowDialog();
            MakeZad();
        }
        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (new addZad(Convert.ToInt64(dataGridView1.SelectedRows[0].Tag))).ShowDialog();
            MakeKal();
        }
        private void dataGridView3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (new addMer(Convert.ToInt64(dataGridView3.SelectedRows[0].Tag))).ShowDialog();
            MakeKal();
        }

        private void dataGridView3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && MessageBox.Show("Вы действительно хотите удалить?","Удаление",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                DataGridViewSelectedRowCollection rows = dataGridView3.SelectedRows;
                db.connect();
                foreach (DataGridViewRow row in rows)
                {
                    db.exec("delete from meropriyatie where id_meropriyatie=" + row.Tag.ToString() + ";");
                    dataGridView3.Rows.Remove(row);
                }
                db.close();
                dataGridView3.ClearSelection();
            }
        }
    }
}
