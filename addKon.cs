﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace eged
{
    public partial class addKon : Form
    {
        public addKon()
        {
            InitializeComponent();
        }
        public addKon(long id)
        {
            this.id = id;
            InitializeComponent();
            db.connect();
            MySqlDataReader reader = db.select("select fam_k,name_k,otch_k,tel_k,email_k,adres_k,data_rogden_k from kontakt where id_kontakt=" + id.ToString() + ";");
            while (reader.Read())
            {
                textBox2.Text = reader.GetString(0) + " " + reader.GetString(1) + " " + reader.GetString(2);
                textBox1.Text = reader.GetString(3);
                textBox3.Text = reader.GetString(4);
                textBox4.Text = reader.GetString(5);
                dateTimePicker1.Value = reader.GetDateTime(6);
            }
            reader.Close();
            db.close();
        }
        private long id = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            db.connect();
            if (id == 0)
                db.exec("insert into kontakt(fam_k, name_k, otch_k, tel_k, email_k, adres_k, data_rogden_k,id_vladelec) values ('" + textBox2.Text.Split(' ')[0] + "', '" + textBox2.Text.Split(' ')[1] + "', '" + textBox2.Text.Split(' ')[2] + "', '" + textBox1.Text + "', '" + textBox3.Text + "', '" + textBox4.Text + "', STR_TO_DATE('" + dateTimePicker1.Value.ToShortDateString() + "', '%d.%m.%Y'), 101);");
            else
                db.exec("update kontakt set fam_k='" + textBox2.Text.Split(' ')[0] + "', name_k='" + textBox2.Text.Split(' ')[1] + "', otch_k='" + textBox2.Text.Split(' ')[2] + "', tel_k='" + textBox1.Text + "', email_k='" + textBox3.Text + "', adres_k='" + textBox4.Text + "', data_rogden_k=STR_TO_DATE('" + dateTimePicker1.Value.ToShortDateString() + "', '%d.%m.%Y') where id_kontakt=" + this.id.ToString() + ";");
            db.close();
        }
    }
}
