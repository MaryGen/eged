﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace eged
{
    public partial class addMer : Form
    {
        public addMer()
        {
            InitializeComponent();
        }
        public addMer(long id)
        {
            this.id = id;
            InitializeComponent();
            db.connect();
            MySqlDataReader reader = db.select("select nazvan_m,nachalo_m,zaverchenie_m,kategor_m from meropriyatie where id_meropriyatie=" + id.ToString() + ";");
            while (reader.Read())
            {
                textBox2.Text = reader.GetString(0);
                dateTimePicker2.Value = reader.GetDateTime(1);
                dateTimePicker1.Value = reader.GetDateTime(2);
                comboBox2.Text = reader.GetString(3);
            }
            reader.Close();
            db.close();
        }
        private long id = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            db.connect();
            if (id == 0)
                db.exec("insert into meropriyatie(nazvan_m,nachalo_m,zaverchenie_m,kategor_m,id_vladelec) values ('" + textBox2.Text + "', STR_TO_DATE('" + dateTimePicker2.Value.ToString() + "', '%d.%m.%Y %H:%i:%s'), STR_TO_DATE('" + dateTimePicker1.Value.ToString() + "', '%d.%m.%Y %H:%i:%s'), '" + comboBox2.Text + "',101);");
            else
                db.exec("update meropriyatie set nazvan_m='" + textBox2.Text + "', nachalo_m=STR_TO_DATE('" + dateTimePicker2.Value.ToString() + "', '%d.%m.%Y %H:%i:%s'), zaverchenie_m=STR_TO_DATE('" + dateTimePicker1.Value.ToString() + "', '%d.%m.%Y %H:%i:%s'), kategor_m='" + comboBox2.Text + "' where id_meropriyatie=" + this.id.ToString() + ";");
            db.close();
        }
    }
}
