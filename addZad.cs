﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace eged
{
    public partial class addZad : Form
    {
        public addZad()
        {
            InitializeComponent();
        }
        public addZad(long id)
        {
            this.id = id;
            InitializeComponent();
            db.connect();
            MySqlDataReader reader = db.select("select nazvan_z,nachalo_z,konez_z,kategor_z,prioritet_z,data_zav_z from zadacha where id_zadache=" + id.ToString() + ";");
            while (reader.Read())
            {
                textBox2.Text = reader.GetString(0);
                dateTimePicker2.Value = reader.GetDateTime(1);
                dateTimePicker1.Value = reader.GetDateTime(2);
                comboBox2.Text = reader.GetString(3);
                comboBox1.Text = reader.GetString(4);
                checkBox1.Checked = reader.GetValue(5) != DBNull.Value;
            }
            reader.Close();
            db.close();
        }
        private long id = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            db.connect();
            if (id == 0)
                db.exec("insert into zadacha(nazvan_z, nachalo_z, konez_z, prioritet_z, kategor_z, data_zav_z,id_vladelec) values ('" + textBox2.Text + "', STR_TO_DATE('" + dateTimePicker2.Value.ToShortDateString() + "', '%d.%m.%Y'), STR_TO_DATE('" + dateTimePicker1.Value.ToShortDateString() + "', '%d.%m.%Y'), " + comboBox1.Text + ", '" + comboBox2.Text + "', " + (checkBox1.Checked ? ("STR_TO_DATE('" + DateTime.Now.ToShortDateString() + "', '%d.%m.%Y')") : "NULL") + ",101);");
            else
                db.exec("update zadacha set nazvan_z='" + textBox2.Text + "', nachalo_z=STR_TO_DATE('" + dateTimePicker2.Value.ToShortDateString() + "', '%d.%m.%Y'), konez_z=STR_TO_DATE('" + dateTimePicker1.Value.ToShortDateString() + "', '%d.%m.%Y'), prioritet_z=" + comboBox1.Text + ", kategor_z='" + comboBox2.Text + "', data_zav_z=" + (checkBox1.Checked ? ("STR_TO_DATE('" + DateTime.Now.ToShortDateString() + "', '%d.%m.%Y')") : "NULL") + " where id_zadache=" + this.id.ToString() + ";");
            db.close();
        }
    }
}
