﻿using MySql.Data.MySqlClient;
using System;

public class db
{
    private static MySqlConnection myConnection;
    public static void connect()
    {
        myConnection = new MySqlConnection("Database=eged;Data Source=192.168.2.100;Port=3306;SslMode=none;User Id=root;CharSet=utf8;Password=Karavay_12");
    }

    public static void close()
    {
        try { myConnection.Close(); } catch { }
    }

    public static MySqlDataReader select(String q)
    {
        MySqlCommand myCommand = new MySqlCommand(q, myConnection);
        myConnection.Open();
        return myCommand.ExecuteReader();
    }
    public static void exec(String q)
    {
        MySqlCommand myCommand = new MySqlCommand(q, myConnection);
        myConnection.Open();
        myCommand.ExecuteNonQuery();
        myConnection.Close();
    }
}