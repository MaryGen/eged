-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 04 2018 г., 19:43
-- Версия сервера: 5.6.38
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `egednevnik`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`%` PROCEDURE `act_zadacha` ()  NO SQL
BEGIN
SELECT NOW();
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `kontakt`
--

CREATE TABLE `kontakt` (
  `id_kontakt` int(11) NOT NULL,
  `fam_k` varchar(255) NOT NULL,
  `name_k` varchar(255) NOT NULL,
  `otch_k` varchar(255) NOT NULL,
  `tel_k` bigint(11) NOT NULL,
  `email_k` text NOT NULL,
  `adres_k` text NOT NULL,
  `data_rogden_k` date NOT NULL,
  `id_vladelec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `kontakt`
--

INSERT INTO `kontakt` (`id_kontakt`, `fam_k`, `name_k`, `otch_k`, `tel_k`, `email_k`, `adres_k`, `data_rogden_k`, `id_vladelec`) VALUES
(111, 'Кривоносова', 'Оля', 'Сергеевна', 89995555654, 'olya@mail.ru', 'Неделина 49', '1998-08-07', 101),
(112, 'Мещерякова', 'Екатерина', 'Владимировна', 89998555674, 'katt@mail.ru', 'Доватора 15', '1996-09-18', 101),
(113, 'Стребкова', 'Елена', 'Александоровна', 89254658975, 'lena@mail.ru', 'Неделина 49', '1998-03-12', 101),
(114, 'Шершнева', 'Ирина', 'Витальевна', 89045698532, 'irr@mail.ru', 'Терешкова 9', '1994-03-16', 101),
(115, 'Иванова', 'Анна', 'Ивановна', 89025631245, 'ann@mail.ru', 'Неделина 45', '1998-12-27', 101),
(116, 'Кирина', 'Ирина', 'Юрьевна', 89995555654, 'ira@mail.ru', 'Луговая 15', '1998-04-25', 101),
(117, 'Филотов', 'Андрей', 'Сергеевич', 89056325987, 'andr@mail.ru', 'Ленина 19', '1977-03-07', 101),
(118, 'Маркова', 'Елизавета', 'Сергеевна', 89569845632, 'lyz@mail.ru', 'Горького 14', '1998-12-25', 101),
(119, 'Щикно', 'Кирилл', 'Витальевич', 89038546213, 'kirl@mail.ru', 'Петра Смородина 19', '1996-05-16', 101),
(120, 'Маркевич', 'Анатасия', 'Андреевна', 89032145696, 'annast@mail.ru', 'Гагарина 84', '1996-07-15', 101);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `kontakt_merop`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `kontakt_merop` (
`id_meropriyatie` int(11)
,`nazvan_m` varchar(255)
,`nachalo_m` datetime
,`zaverchenie_m` datetime
,`kategor_m` varchar(255)
,`id_kontakt` int(11)
,`fam_k` varchar(255)
,`name_k` varchar(255)
,`otch_k` varchar(255)
,`tel_k` bigint(11)
,`email_k` text
,`adres_k` text
,`data_rogden_k` date
);

-- --------------------------------------------------------

--
-- Структура таблицы `meropriyatie`
--

CREATE TABLE `meropriyatie` (
  `id_meropriyatie` int(11) NOT NULL,
  `nazvan_m` varchar(255) NOT NULL,
  `nachalo_m` datetime NOT NULL,
  `zaverchenie_m` datetime NOT NULL,
  `kategor_m` varchar(255) NOT NULL,
  `id_vladelec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meropriyatie`
--

INSERT INTO `meropriyatie` (`id_meropriyatie`, `nazvan_m`, `nachalo_m`, `zaverchenie_m`, `kategor_m`, `id_vladelec`) VALUES
(201, 'Пара', '2018-03-12 08:00:00', '2018-03-12 09:30:00', 'Учеба', 101),
(202, 'Встреча', '2018-03-14 10:42:23', '2018-03-15 13:24:19', 'Личное', 101),
(203, 'Концерт', '2017-12-20 17:19:41', '2017-12-21 03:12:09', 'Прочее', 101),
(204, 'Уборка', '2018-05-01 08:29:16', '2018-05-01 13:19:29', 'Дом', 101),
(205, 'Семинар', '2018-02-08 12:19:35', '2018-02-08 18:30:18', 'Учеба', 101),
(206, 'Пара', '2018-06-21 11:21:33', '2018-06-21 14:24:29', 'Учеба', 101),
(208, 'Концерт', '2018-05-23 14:22:28', '2018-05-24 10:22:00', 'Прочее', 101),
(209, 'Поход в парк', '2018-05-09 08:44:00', '2018-05-09 20:37:00', 'Отдых', 101),
(210, 'День Рождение', '2018-04-02 06:00:00', '2018-04-03 20:00:00', 'Прочее', 101);

-- --------------------------------------------------------

--
-- Структура таблицы `meropriyatie_kontakt`
--

CREATE TABLE `meropriyatie_kontakt` (
  `id_meropriyatie` int(11) NOT NULL,
  `id_kontakt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meropriyatie_kontakt`
--

INSERT INTO `meropriyatie_kontakt` (`id_meropriyatie`, `id_kontakt`) VALUES
(201, 111),
(202, 112),
(203, 113),
(204, 114),
(205, 115),
(206, 116),
(207, 117),
(208, 118),
(209, 119),
(210, 120);

-- --------------------------------------------------------

--
-- Структура таблицы `vladelec`
--

CREATE TABLE `vladelec` (
  `id_vladelec` int(11) NOT NULL,
  `fam_v` varchar(20) NOT NULL,
  `name_v` varchar(20) NOT NULL,
  `otch_v` varchar(20) NOT NULL,
  `data_rogden` date NOT NULL,
  `tel_v` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vladelec`
--

INSERT INTO `vladelec` (`id_vladelec`, `fam_v`, `name_v`, `otch_v`, `data_rogden`, `tel_v`) VALUES
(101, 'Генералова', 'Мария', 'Сергеевна', '1998-08-08', 89995632147);

-- --------------------------------------------------------

--
-- Структура таблицы `zadacha`
--

CREATE TABLE `zadacha` (
  `id_zadache` int(11) NOT NULL,
  `nazvan_z` varchar(255) NOT NULL,
  `nachalo_z` date NOT NULL,
  `konez_z` date NOT NULL,
  `prioritet_z` int(2) NOT NULL,
  `kategor_z` varchar(255) NOT NULL,
  `data_zav_z` date DEFAULT NULL,
  `id_vladelec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zadacha`
--

INSERT INTO `zadacha` (`id_zadache`, `nazvan_z`, `nachalo_z`, `konez_z`, `prioritet_z`, `kategor_z`, `data_zav_z`, `id_vladelec`) VALUES
(401, 'Лабораторная', '2018-03-01', '2018-03-12', 1, 'Учеба', '2018-03-10', 101),
(402, 'Курсовая', '2018-03-20', '2018-07-12', 2, 'Учеба', '2018-06-20', 101),
(403, 'Починка крана', '2018-05-18', '2018-05-30', 1, 'Дом', '2018-06-03', 101),
(404, 'Написать книгу', '2017-11-01', '2018-10-12', 3, 'Работа', '2018-07-10', 101),
(405, 'Ремонт на кухни', '2018-10-11', '2018-12-12', 3, 'Дом', '2018-11-30', 101),
(406, 'Домашнее задание', '2018-05-01', '2018-05-07', 1, 'Учеба', '2018-05-10', 101),
(407, 'Придумать название книги', '2017-11-01', '2018-01-30', 2, 'Работа', '0000-00-00', 101),
(408, 'Оплатить счета', '2018-04-01', '2018-04-22', 1, 'Дом', '2018-04-15', 101),
(409, 'Поздравить подруженьку', '2018-08-07', '2018-08-07', 1, 'Личное', '2018-08-08', 101),
(410, 'Организовать свое День Рождение', '2018-07-15', '2018-08-08', 2, 'Личное', '2018-08-05', 101),
(411, 'Пара', '2018-06-01', '2019-01-31', 2, 'Учеба', NULL, 101);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `zadacha_vlad`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `zadacha_vlad` (
`id_zadache` int(11)
,`nazvan_z` varchar(255)
,`nachalo_z` date
,`konez_z` date
,`prioritet_z` int(2)
,`kategor_z` varchar(255)
,`data_zav_z` date
,`id_vladelec` int(11)
,`fam_v` varchar(20)
,`name_v` varchar(20)
,`otch_v` varchar(20)
,`data_rogden` date
,`tel_v` bigint(11)
);

-- --------------------------------------------------------

--
-- Структура таблицы `zametka`
--

CREATE TABLE `zametka` (
  `id_zametka` int(11) NOT NULL,
  `text_za` text NOT NULL,
  `kategor_za` varchar(255) NOT NULL,
  `id_vladelec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zametka`
--

INSERT INTO `zametka` (`id_zametka`, `text_za`, `kategor_za`, `id_vladelec`) VALUES
(301, 'Студвесна', 'Учеба', 101),
(302, 'День Рождение моей подруженьки Ольчи', 'Праздники', 101),
(303, 'Здача лабораторной работы', 'Учеба', 101),
(304, 'Пароль от Ви-Фи:256359874', 'Дом', 101),
(305, 'Подарки для моей подруженьки: 1)Цветы (обязательно); 2) Много вкусных кофет (но не обязательно); 3)Что-то очень крутое на память', 'Праздники', 101),
(306, 'Покупки: 1)Кофточку; 2)Платишко; 3)Помаду ', 'Личное', 101),
(307, 'Сериалы: 1)Как я встретил вашу маму; 2) Твин Пикс; 3)Сверхестественное; 4)Теория Большого взрыва; 5)Ривендэил', 'Прочее', 101),
(308, 'Фильмы: 1)Мстители; 2)Груз 200; 3)Мрачные тени; 4)Сабибор; 5)Дюнкер', 'Прочее', 101),
(309, 'Покупки: 1)мыло; 2)тарелки; 3)полотенце; 4)ножи; 5)постельное белье', 'Дом', 101),
(310, 'Получить заказ из интернет магазина', 'Личное', 101);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `zametka_shop`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `zametka_shop` (
`id_zametka` int(11)
,`text_za` text
,`kategor_za` varchar(255)
);

-- --------------------------------------------------------

--
-- Структура для представления `kontakt_merop`
--
DROP TABLE IF EXISTS `kontakt_merop`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `kontakt_merop`  AS  select `m`.`id_meropriyatie` AS `id_meropriyatie`,`m`.`nazvan_m` AS `nazvan_m`,`m`.`nachalo_m` AS `nachalo_m`,`m`.`zaverchenie_m` AS `zaverchenie_m`,`m`.`kategor_m` AS `kategor_m`,`k`.`id_kontakt` AS `id_kontakt`,`k`.`fam_k` AS `fam_k`,`k`.`name_k` AS `name_k`,`k`.`otch_k` AS `otch_k`,`k`.`tel_k` AS `tel_k`,`k`.`email_k` AS `email_k`,`k`.`adres_k` AS `adres_k`,`k`.`data_rogden_k` AS `data_rogden_k` from ((`meropriyatie_kontakt` `mk` join `meropriyatie` `m`) join `kontakt` `k`) where ((`mk`.`id_meropriyatie` = `m`.`id_meropriyatie`) and (`mk`.`id_kontakt` = `k`.`id_kontakt`)) ;

-- --------------------------------------------------------

--
-- Структура для представления `zadacha_vlad`
--
DROP TABLE IF EXISTS `zadacha_vlad`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `zadacha_vlad`  AS  select `z`.`id_zadache` AS `id_zadache`,`z`.`nazvan_z` AS `nazvan_z`,`z`.`nachalo_z` AS `nachalo_z`,`z`.`konez_z` AS `konez_z`,`z`.`prioritet_z` AS `prioritet_z`,`z`.`kategor_z` AS `kategor_z`,`z`.`data_zav_z` AS `data_zav_z`,`v`.`id_vladelec` AS `id_vladelec`,`v`.`fam_v` AS `fam_v`,`v`.`name_v` AS `name_v`,`v`.`otch_v` AS `otch_v`,`v`.`data_rogden` AS `data_rogden`,`v`.`tel_v` AS `tel_v` from (`zadacha` `z` join `vladelec` `v`) where (`v`.`id_vladelec` = `z`.`id_vladelec`) ;

-- --------------------------------------------------------

--
-- Структура для представления `zametka_shop`
--
DROP TABLE IF EXISTS `zametka_shop`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `zametka_shop`  AS  select `z`.`id_zametka` AS `id_zametka`,`z`.`text_za` AS `text_za`,`z`.`kategor_za` AS `kategor_za` from `zametka` `z` where (`z`.`kategor_za` = 'Личное') ;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`id_kontakt`);

--
-- Индексы таблицы `meropriyatie`
--
ALTER TABLE `meropriyatie`
  ADD PRIMARY KEY (`id_meropriyatie`);

--
-- Индексы таблицы `vladelec`
--
ALTER TABLE `vladelec`
  ADD PRIMARY KEY (`id_vladelec`);

--
-- Индексы таблицы `zadacha`
--
ALTER TABLE `zadacha`
  ADD PRIMARY KEY (`id_zadache`);

--
-- Индексы таблицы `zametka`
--
ALTER TABLE `zametka`
  ADD PRIMARY KEY (`id_zametka`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
